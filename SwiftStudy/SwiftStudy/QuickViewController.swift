//
//  QuickViewController.swift
//  SwiftStudy
//
//  Created by 黄成瑞 on 2020/6/23.
//  Copyright © 2020 xiaoxiao. All rights reserved.
//  Swift知识点快速浏览

import UIKit

class QuickViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.red
        // Do any additional setup after loading the view.
        
        test1()
//        print(greet(person: "小黄", day: "星期一"))
//        print(greet("大黄", on: "星期二"))
//        print(calculateStatistics(scores: [5, 3, 100, 3, 9]))
//        print(returnFifteen())
//        var incrementFunc = makeIncrementer()
//        print(incrementFunc(7))
//        print(makeIncrementer())
//        let numbers = [20, 19, 7, 12]
//        print(hasAnyMatches(list: numbers, condition: lessThanTen))
//        let reslut = numbers.map({
//            (number: Int) -> Int in
//            let result = 3 * number
//            return result
//        })
//        print(reslut)
//        let result = numbers.map({number in 3 * number})
//        print(result)
//        let sortedNumbers = numbers.sorted {( $0 > $1 )}
//        print(sortedNumbers)
//        var shape = NamedShape(name: "大大大黄黄")
//        shape.numberOfSides = 7
//        var shapeDescription = shape.simpleDescription()
//        print("\(shape.numberOfSides) --- \(shapeDescription) --- \(shape.name ?? "额。。。木有值哦")")
//        let test = Square(sideLength: 5.2, name: "小小咩")
//        print("\(test.area()) --- \(test.simpleDescription()) --- \(test.sideLength)")
//        test.area()
//        test.simpleDescription()
//        var triangle = EquilateralTriangle(sideLength: 3.1, name: "a triangle")
//        print(triangle.perimeter1)
//        triangle.perimeter1 = 9.9
//        print(triangle.sideLength)
//        print(triangle.perimeter1)
//        var triangleAndSquare = TriangleAndSquare(size: 10, name: "another test shape")
//        print(triangleAndSquare.square.sideLength)
//        print(triangleAndSquare.triangle.sideLength)
//        triangleAndSquare.square = Square(sideLength: 50, name: "larger square")
//        print(triangleAndSquare.triangle.sideLength)
//        let optionalSquare: Square? = Square(sideLength: 2.5, name: "optional square")
//        let sideLength = optionalSquare?.sideLength
    }

    func test1() {
        
        /*
        print("朋友你好～")
        var a = 42
        a = 50
        let b = 30
        print("\(a)...\(b)")
        */
        
        /*
        let a: String
        var b: String
        let c: Double = 70
        let d: Float = 1
        a = "我是a"
        b = "我是b"
        print("\(a)...\(b)...\(c)...\(d)")
        */
        
        /*
        let a = "What?"
        let b = 88
        let ab = a + String(b)
        let abNew = "Look \(a) \(b)"
        print(a + "...\(b)..." + ab + abNew)
        */
        
        /*
        let a = 3
        let b = 5
        let aLog = "I have \(a)"
        let bLog = "I have \(a + b)"
        let c = 0.6
        let d = "Hello"
        let e = "\(c)d"
        print("\(a)...\(b)...\(aLog)...\(bLog)...\(c)...\(d)...\(e)")
        */
        
        /*
        let a = 2
        let b = 3
        let c = """
        I Love You ... \(a) ... \(b)
        
        
            Your Love \(a + b)
        """
        print("\(a)...\(b)...\(c)")
        */
        
        /*
        var a = ["1", "2", "3", "4", "5"]
        a[1] = "8"
        a.append("6")
        var b = [
            "key1":"value1",
            "key2":"value2",
            "key3":"value3"
        ]
        b["key2"] = "被改了"
        b.updateValue("value5", forKey: "key5")
        print("\(a) \n \(b)")
        */
        
        /*
        let arr = [String]()
        let dict = [String:Float]()
        */
        
        /*
        a = []
        b = [:]
        */
        
        /*
        let array = [1, 2, 3, 4, 5]
        var a = 0
        for i in array {
            if i > 3 {
                a += 1
            } else {
                a += 3
            }
        }
        print(a)
        */
        
        /*
        var a: String? = "111"
        a = nil
        print(a == nil)
        var b: String? = "你好"
        var c = "谢谢"
        if let d = b {
            c = "nihao, \(d)"
        }
        print(c)
        */
    }
    
    /*
    func greet(person: String, day: String) -> String {
        return "Hello \(person), today is \(day)"
    }
    */
    
    /*
    func greet(_ person: String, on day: String) -> String {
        return "Hello \(person), today is \(day)"
    }
    */
   
    /*
    func calculateStatistics(scores: [Int]) -> (min: Int, max: Int, sum: Int) {
        var min = scores[0]
        var max = scores[0]
        var sum = 0
        for score in scores {
            if score < min {
                min = score
            } else if score > max {
                max = score
            }
            sum += score
        }
        return (min, max, sum)
    }
     */
    
    /*
    func returnFifteen() -> Int {
        var y = 10
        func add() {
            y += 5
        }
        add()
        return y
    }
     */
    
    /*
    func makeIncrementer() -> ((Int) -> Int) {
        func addOne(number: Int) -> Int {
            return 1 + number
        }
        return addOne
    }
     */
    
    /*
    func hasAnyMatches(list:[Int], condition: (Int) -> Bool) -> Bool {
        for item in list {
            if condition(item) {
                return true
            }
        }
        return false
    }
    func lessThanTen(number: Int) -> Bool {
        return number < 10
    }
     */
}


class NamedShape {
    var numberOfSides: Int? = 0
    var name: String?
    
    init(name: String) {
        self.name = name
        self.name = nil
    }
    
    func simpleDescription() -> String {
//        return "A shape With \(String(describing: numberOfSides)) sides."
        return "A shape With \(name ?? "没有值～") sides."
    }
    
    deinit {
        self.name = nil
        numberOfSides = nil
    }
    
}

class Square: NamedShape {
    var sideLength: Double
    
    init(sideLength: Double, name: String) {
        self.sideLength = sideLength
        super.init(name: name)
        numberOfSides = 5
    }
    
    func area() -> Double {
        return sideLength * sideLength
    }
    
    override func simpleDescription() -> String {
        return "A square with sides of length \(sideLength)."
    }
}

class EquilateralTriangle: NamedShape {
    var sideLength: Double = 0.0
    
    init(sideLength: Double, name: String) {
        self.sideLength = sideLength
        super.init(name: name)
        numberOfSides = 3
    }
    var perimeter1: Double {
        get {
            return 3.0 * sideLength
        }
        set {
            sideLength = newValue / 3.0
        }
    }
    override func simpleDescription() -> String {
        return "An equilateral triangle with sides of length \(sideLength)."
    }
}

class TriangleAndSquare {
    var triangle: EquilateralTriangle {
        willSet {
            square.sideLength = newValue.sideLength
        }
    }
    var square: Square {
        willSet {
            triangle.sideLength = newValue.sideLength
        }
    }
    init(size: Double, name: String) {
        square = Square(sideLength: size, name: name)
        triangle = EquilateralTriangle(sideLength: size, name: name)
    }
}

